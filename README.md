# WindowGrid #
## WindowGrid is a modern window management program for Windows. ##

When moving a window by the title bar, simply right click to start sizing the window from the cursor.

Move cursor to resize the window, and let go of left click when window is as desired.

Website: [windowgrid.net](http://windowgrid.net "WindowGrid's Homepage")

### Examples ###

![WindowGrid-thumb.gif](https://bitbucket.org/repo/AyEyMa/images/1160337051-WindowGrid-thumb.gif)

***

![Example-2160p.png](https://bitbucket.org/repo/AyEyMa/images/1067901070-Example-2160p.png)

### Change Log ###

#### 1.3.1.1
	FIXED: Re-added "Hardcodet.Wpf.TaskbarNotification.dll".

#### 1.3.1.0
	ADDED: Transposing of grid when displaying on a monitor in portrait orientation (Height > Width).
	MODIFIED: Re-added recommended OS settings.
	MODIFIED: Moved overlay code into WindowGridOverlay.
	FIXED: Should now work on pre-Windows 8 again.
	FIXED: Removed update connection warning on startup.

#### 1.3.0.0
	ADDED: Input customization.
	UPDATED: Now using WPF in place of WinForms.
	MODIFIED: New Interface.
	FIXED: Install related issues on specific locales (Was related to directory security).
	FIXED: A few DPI related issues.
	FIXED: Hook related issues (i.e. Tab tearing in Chrome).
	FIXED: Many other issues.

#### 1.2.9.1
	FIXED: 32bit OS issues.

#### 1.2.9.0
	ADDED: Spacebar as an alternative to right click.
	ADDED: Move window mode when pressing middle click.
	MODIFIED: Drastically simplified the hook process.
	FIXED: Numerous issues.

#### 1.2.8.2
	FIXED: Some install issues.
	FIXED: Auto start should now work correctly on Windows 7.
	FIXED: Small hook related problems.

#### 1.2.8.1
	FIXED: Had issues updating from a path with spaces.

#### 1.2.8.0
	ADDED: "Cover Window Overlay" option.
	ADDED: Install Dialog.
	MODIFIED: "WindowGrid.exe" will now use its running location as it's working directory rather than "ProgramData" (Portable).
	FIXED: Some hook related issues.

#### 1.2.7.0
	ADDED: Option to disable splash screen.
	ADDED: Ability to change the grid colors from the context menu.
	ADDED: About menu.

#### 1.2.6.1
	ADDED: Option to enable showing of window contents while dragging windows.
	UPDATED: Context Menu layout.

#### 1.2.6.0
	UPDATED: Compiled using VS2015 from VS2013.
	FIXED: Window borders in Windows 10 display correctly now.

#### 1.2.5.0
	ADDED: Auto updating on startup.
	ADDED: Windows 10 support.
	ADDED: Preliminary command line argument support.
	FIXED: Some scheduling related issues.
	NOTE: Some windows in Windows 10 may not display correctly.

#### 1.2.4.1
	ADDED: More Logging.
	FIXED: Crash when using incorrect working directory.
	FIXED: Auto start issues.

#### 1.2.4.0
	FIXED: Some performance and graphical issues related to the Grid.
	FIXED: Small hook related issue.
	MODIFIED: Unified the log files.

#### 1.2.3.0
	ADDED: Update check.
	FIXED: Visual Studio would crash when moving floating windows.

#### 1.2.2.1
	FIXED: Steam compatibility.

#### 1.2.2.0
	FIXED: DLL now detaches from hooked process properly.
	FIXED: Some startup issues.

#### 1.2.1.1
	FIXED: A single instance issue.

#### 1.2.1.0
	ADDED: Install button in settings, installs the exe and adds shortcut to start menu.
	MODIFIED: Hook dlls now stored in internal zip file.
	MODIFIED: Compressed icon.

#### 1.2.0.1
	FIXED: WindowGrid wouldn't activate if dragged from a maximized window.

#### 1.2.0.0
	ADDED: New logo and color theme.
	ADDED: Customizable colors and opacity in 'Settings.xml'.
	ADDED: Logger to the C# component.
	ADDED: Supported OS' to the manifest.
	FIXED: Some issues.

#### 1.1.0.0
	UPDATED: Grid renderer (Better layout and new start/end point indicators).
	FIXED: Some performance issues.

#### 1.0.3.0
	ADDED: Grid renderer.
	MODIFIED: Hold right click to select starting grid, let go to start sizing window.

#### 1.0.2.0
	UPDATED: Visual Studio redists are now statically linked (Shouldn't be required).

#### 1.0.1.42191
	UPDATED: Context menu.
	UPDATED: Now a single file, settings are now stored in user directory.
	ADDED: Disabling of Windows Aero Shake in settings.

#### 1.0.0.0
	ADDED: Initial Commit.

	
### Old ###

![Anim.gif](https://bitbucket.org/repo/AyEyMa/images/686211226-Anim.gif Example)

![Example.png](https://bitbucket.org/repo/AyEyMa/images/534543515-Example.png Example)